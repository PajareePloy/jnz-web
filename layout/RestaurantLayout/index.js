import Head from 'next/head'
import { withStyles } from '@material-ui/core/styles'

import RestaurantHeader from '../../components/RestaurantHeader'
import RestaurantFooter from '../../components/RestaurantFooter'

import styles from './styles'

const RestaurantLayout = ({ children, classes }) => (
    <div className={ classes.container } >
        <Head><title>Jenosize</title></Head>
        { <RestaurantHeader /> }
        { children }
        { <RestaurantFooter /> }
    </div>
)

export default withStyles(styles)(RestaurantLayout)