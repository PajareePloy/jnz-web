import Head from 'next/head'
import { withStyles } from '@material-ui/core/styles'

import Header from '../../components/Header'
import Footer from '../../components/Footer'

import styles from './styles'

const ReportLayout = ({ children, hideFooter = false, classes }) => (
    <div className={ classes.container } >
        <Head><title>Jenosize</title></Head>
        { <Header /> }
        { children }
        { hideFooter ? '' : <Footer /> }
    </div>
)

export default withStyles(styles)(ReportLayout)