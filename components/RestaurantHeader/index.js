import React from 'react'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles'
import { Paper, Table, TableRow, TableCell, Typography, IconButton, InputBase } from '@material-ui/core'
import { Search } from '@material-ui/icons'
import styles from './styles'

import { RESTAURANT_DATA } from '../../redux/actions/types'
import apiClient from '../../apis'

class RestaurantHeader extends React.Component {
 
    constructor(props) {
        super(props)
        this.state = {
            searchText: ''
        }
    }

    handleChange = (name) => (event) => {
        this.setState({
            [name]: event.target.value,
        })
    }

    searchRestaurant = async() => {
        const data = await apiClient.restaurant.searchRestaurant(this.state.searchText)
        this.props.dispatch({ type: RESTAURANT_DATA, restaurantData: data })
    }

    render() {
        const { classes } = this.props
        const { searchText } = this.state
        return (
            <div className={classes.container}>
                <Table>
                    <TableRow>
                        <TableCell className={classes.picContainer}>
                            <Paper className={classes.paper} />
                        </TableCell>
                        <TableCell>
                            <Typography variant='h3'>Website Name</Typography>
                        </TableCell>
                        <TableCell align='right' className={classes.searchContainer}>
                            <Paper component='form' className={classes.searchRoot}>
                                <InputBase
                                    className={classes.input}
                                    value={searchText}
                                    onChange={this.handleChange('searchText')}
                                    placeholder='Search Restaurant'
                                />
                                <IconButton className={classes.iconButton} onClick={()=>this.searchRestaurant()} >
                                    <Search />
                                </IconButton>
                            </Paper>
                        </TableCell>
                    </TableRow>
                </Table>
            </div>
        )
    } 
}

export default connect(state => state)(withStyles(styles)(RestaurantHeader))