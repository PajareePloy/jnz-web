export default (theme) => ({
    container: {
        position: 'absolute',
        overflow: 'hidden',
        top: '0px',
        left: '0px',
        right: '0px'
    },
    paper: {
        width: '100px',
        height: '100px'
    },
    picContainer: {
        padding: '20px',
        width: '150px'
    },
    searchRoot: {
        padding: '2px 4px',
        display: 'flex',
        alignItems: 'center',
        width: 400
    },
    searchContainer: {
        width: 500
    },
    textSearch: {
        height: '100px'
    },
    input: {
        marginLeft: theme.spacing(1),
        flex: 1
    },
    iconButton: {
        padding: 10,
    }
})