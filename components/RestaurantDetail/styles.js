export default (theme) => ({
    container: {
        marginLeft: '20%',
        marginRight: '20%',
        width: '50%'
    },
    paper: {
        width: '200px',
        height: '200px'
    },
    picContainer: {
        padding: '20px',
        width: '250px',
        border: '0px'
    }
})