import React from 'react'
import { withStyles } from '@material-ui/core/styles'
import { Table, TableCell, TableRow, Typography, Paper } from '@material-ui/core'
import styles from './styles'

class RestaurantDetail extends React.Component {

    render() {
        const { classes, title, detail } = this.props
        return (
            <Table className={classes.container}>
                <TableRow>
                    <TableCell className={classes.picContainer}>
                        <Paper className={classes.paper} />
                    </TableCell>
                    <TableCell>
                        <Typography variant='h5'>{title}</Typography>
                        <Typography variant='body2'>{detail}</Typography>
                    </TableCell>
                </TableRow>
            </Table>
        )
    } 
}
export default (withStyles(styles)(RestaurantDetail))