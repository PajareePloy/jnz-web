export default (theme) => ({
    container: {
        position: 'absolute',
        overflow: 'hidden',
        top: '0px',
        left: '0px',
        right: '0px'
    },
    menuButton: {
        marginRight: theme.spacing(2)
    },
    title: {
        flexGrow: 1,
        textAlign:'center'
    },
    whiteColor: {
        color: 'white'
    },
    mainMenu: {
        width: '60%'
    },
    toolbar: {
        minHeight: 100,
        paddingTop: '40px',
        paddingBottom: '0px'
    },
    linkToBack: {
        textDecoration: 'none'
    }
})