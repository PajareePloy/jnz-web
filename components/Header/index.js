import React from 'react'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles'
import { AppBar, Toolbar, IconButton, Select, MenuItem, Link } from '@material-ui/core'
import { Home, Menu, MoreHoriz } from '@material-ui/icons'
import styles from './styles'

import { CHANGE_REPORT_TYPE } from '../../redux/actions/types'

class Header extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            reportType: 'All',
        }
    }

    componentDidMount() {
        this.changeReportType(this.state.reportType)
    }

    componentWillReceiveProps = (nextProps) => {
        this.setState({
            reportType: nextProps.report.reportType
        })
    }

    handleChange = (event) => {
        this.changeReportType(event.target.value)
    }

    changeReportType = (value) => {
        this.props.dispatch({ type: CHANGE_REPORT_TYPE, reportType: value })
    }

    renderLeftIcon() {
        const { classes } = this.props
        const { reportType } = this.state
        if (reportType === 'All') {
            return (
                <IconButton edge='start' className={classes.menuButton} color='inherit'>
                    <Home />
                </IconButton>
            )
        }
        else {
            return (
                <Link href='#' onClick={()=>this.changeReportType('All')} color='inherit' className={classes.linkToBack} >{'< Back'}</Link>
            )
        }
    }

    render() {
        const { classes } = this.props
        const { reportType } = this.state
        return (
            <div className={classes.container}>
                <AppBar position='static'>
                    <Toolbar className={classes.toolbar}>
                        { this.renderLeftIcon() }
                        <div className={classes.title} >
                            <Select value={reportType} onChange={this.handleChange} disableUnderline className={classes.mainMenu} classes={{root: classes.whiteColor, icon: classes.whiteColor}}>
                                <MenuItem value='All'>All Report</MenuItem>
                                <MenuItem value='OKR'>OKRs Report</MenuItem>
                            </Select>
                        </div>
                        { reportType === 'All' ? <Menu /> : <MoreHoriz />}
                    </Toolbar>
                </AppBar>
            </div>
        )
    } 
}

export default connect(state => state)(withStyles(styles)(Header))