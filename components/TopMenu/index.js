import React from 'react'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles'
import { Tabs, Tab, Table, TableHead, TableRow, TableCell, Typography, IconButton } from '@material-ui/core'
import { Equalizer, List, ArrowDownward, PresentToAll, CalendarToday, Publish } from '@material-ui/icons'
import moment from 'moment'
import DayPickerInput from 'react-day-picker/DayPickerInput'
import DayPicker from 'react-day-picker'
import styles from './styles'
import 'react-day-picker/lib/style.css'

import { CHANGE_DATE, CHANGE_VIEW_TYPE } from '../../redux/actions/types'


class TopMenu extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            selectedMenu: 'Engagement',
            dateType: 'Daily',
            viewType: 'Graph',
            startDate: '',
            endDate: '',
            showCalendar: false,
            hoverRange: undefined,
            selectedDays: []
        }
        this.handleDateChange = this.handleDateChange.bind(this)
    }

    componentDidMount() {
        const newDate = moment().format('DD MMM YYYY')
        this.changeDate(newDate, newDate, 'Daily')
    }

    handleDateChange(day) {
        const selectedDate = moment(day).format('DD MMM YYYY')
        this.changeDate(selectedDate, selectedDate, 'Daily')
    }

    handledDateTypeChange(type) {
        this.setState({
            dateType: type,
            hoverRange: undefined,
            selectedDays: [],
        })
        const newDate = moment().format('DD MMM YYYY')
        this.changeDate(newDate, newDate, type)
        this.handleChangeViewType('Graph')
    }

    changeDate(startDate, endDate, dateType) {
        this.setState({ 
            startDate: startDate,
            endDate: endDate
        })
        this.props.dispatch({ type: CHANGE_DATE, dateType: dateType, startDate: startDate, endDate: endDate })
    }

    changeCalendar() {
        const old = this.state.showCalendar
        this.setState({
            showCalendar: old ? false : true
        })
    }

    resetToday() {
        const dateType = 'Daily'
        this.setState({
            dateType: dateType,
        })
        const newDate = moment().format('DD MMM YYYY')
        this.changeDate(newDate, newDate, dateType)
    }

    handleDayChange = (date) => {
        const rangeDate = getWeekRange(date)
        const startDate = moment(rangeDate.from).format('DD MMM YYYY')
        const endDate = moment(rangeDate.to).format('DD MMM YYYY')
        this.setState({
            selectedDays: getWeekDays(rangeDate.from)
        })
        this.changeDate(startDate, endDate, 'Weekly')
    }

    handleDayEnter = date => {
        this.setState({
            hoverRange: getWeekRange(date),
        })
    }
    
    handleDayLeave = () => {
        this.setState({
            hoverRange: undefined,
        })
    }
    
    handleWeekClick = (weekNumber, days, e) => {
        this.setState({
            selectedDays: days,
        })
    }

    handleChangeViewType(value) {
        this.setState({
            viewType: value
        })
        this.props.dispatch({ type: CHANGE_VIEW_TYPE, viewType: value })
    }

    renderMenu() {
        const { selectedMenu } = this.state
        return (
            <Table>
                <TableHead center>
                    <TableRow>
                        <TableCell align='center'>
                            <Typography variant='subtitle2' color={selectedMenu === 'Submission' ? 'primary' : 'secondary'} >Submission</Typography>
                        </TableCell>
                        <TableCell align='center'>
                            <Typography variant='subtitle2' color='secondary'>|</Typography>
                        </TableCell>
                        <TableCell align='center'>
                            <Typography variant='subtitle2' color={selectedMenu === 'Engagement' ? 'primary' : 'secondary'}>Engagement</Typography>
                        </TableCell>
                    </TableRow>
                </TableHead>
            </Table>
        )
    }

    renderCalendar() {
        const { classes } = this.props
        const { dateType, viewType, startDate, endDate, showCalendar } = this.state
        return (
            <Table>
                <TableRow>
                    <TableCell className={classes.tabCell}>
                        <Tabs value={dateType} variant='fullWidth' indicatorColor='primary' textColor='primary'>
                            <Tab label='Daily' value='Daily' className={classes.tab} onClick={()=>this.handledDateTypeChange('Daily')} />
                            <Tab label='Weekly' value='Weekly' className={classes.tab} onClick={()=>this.handledDateTypeChange('Weekly')}/>
                            <Tab label='Monthly' value='Monthly' className={classes.tab} />
                        </Tabs>
                    </TableCell>
                    <TableCell className={classes.iconLeftCell}>
                        <IconButton color={viewType === 'Graph'?'primary':'secondary'} onClick={()=>this.handleChangeViewType('Graph')} component='span' className={classes.zeroPadding}>
                            <Equalizer />
                        </IconButton>
                    </TableCell>
                    <TableCell className={classes.iconCell}>
                        <IconButton color={viewType === 'List'?'primary':'secondary'} onClick={()=>this.handleChangeViewType('List')} component='span' className={classes.zeroPadding}>
                            <List />
                        </IconButton>
                    </TableCell>
                </TableRow>
                { !showCalendar ? 
                <TableRow>
                    <TableCell align='center' className={classes.datePicker}>
                        { dateType === 'Daily' ? this.renderDailyPicker() : this.renderWeeklyPicker() }
                    </TableCell>
                    <TableCell colSpan={2} className={classes.calendar}>
                        <IconButton color='primary' component='span' className={classes.zeroPadding} onClick={()=>this.resetToday()}>
                            <ArrowDownward fontSize='small' />
                            <Typography variant='subtitle2' color='primary'>Today</Typography>
                        </IconButton>
                    </TableCell>
                </TableRow> : this.renderWeeklyPickerCalendar()}
                <TableRow>
                    <TableCell className={classes.dateSelected}>
                        <Typography variant='subtitle2' color='secondary'>{startDate}{dateType==='Weekly'?` - ${endDate}` : ''}</Typography>
                    </TableCell>
                    <TableCell colSpan={2} className={classes.today}>
                        <IconButton color='primary' component='span' className={classes.zeroPadding}>
                            <PresentToAll />
                        </IconButton>
                    </TableCell>
                </TableRow>
            </Table>
        )
    }

    renderDailyPicker() {
        const { startDate } = this.state
        return ( 
            <DayPickerInput format='DD MMM YYYY' value={startDate} onDayChange={this.handleDateChange} />  
        )
    }

    renderWeeklyPicker() {
        return (
            <IconButton component='span' onClick={()=>this.changeCalendar()}>
                <CalendarToday fontSize='small' />
            </IconButton>
        )
    }

    renderWeeklyPickerCalendar() {
        const { hoverRange, selectedDays } = this.state
        const daysAreSelected = selectedDays.length > 0
    
        const modifiers = {
            hoverRange,
            selectedRange: daysAreSelected && {
                from: selectedDays[0],
                to: selectedDays[6],
            },
            hoverRangeStart: hoverRange && hoverRange.from,
            hoverRangeEnd: hoverRange && hoverRange.to,
            selectedRangeStart: daysAreSelected && selectedDays[0],
            selectedRangeEnd: daysAreSelected && selectedDays[6]
        }

        return (
            <TableRow>
                <TableCell colSpan={3} align='center'>
                    <DayPicker
                        selectedDays={selectedDays}
                        showOutsideDays
                        modifiers={modifiers}
                        onDayClick={this.handleDayChange}
                        onDayMouseEnter={this.handleDayEnter}
                        onDayMouseLeave={this.handleDayLeave}
                        onWeekClick={this.handleWeekClick} />
                    <br />
                    <IconButton component='span' onClick={()=>this.changeCalendar()}>
                        <Publish fontSize='small' />   
                    </IconButton>
                </TableCell>
            </TableRow>
        )
    }

    render() {
        return (
            <div>
                { this.renderMenu() }
                { this.renderCalendar() }
            </div>
        )
    } 
}

export default connect(state => state)(withStyles(styles)(TopMenu))


function getWeekDays(weekStart) {
    const days = [weekStart]
    for (let i = 1; i < 7; i += 1) {
        days.push(moment(weekStart).add(i, 'days').toDate())
    }
    return days
}
  
function getWeekRange(date) {
    return { 
        from: moment(date).startOf('week').toDate(),
        to: moment(date).endOf('week').toDate(),
    }
}