export default (theme) => ({
    tabCell: {
        padding: '0px',
        borderBottom: '0px',
    },
    tab: {
        textTransform: 'none',
        borderBottom: `solid ${theme.palette.grey[400]} 1px`
    },
    iconCell: {
        borderBottom: '0px',
        paddingLeft: '0px',
        paddingRight: '5px'
    },
    iconLeftCell: {
        borderBottom: '0px',
        paddingLeft: '20px',
        paddingRight: '5px'
    },
    zeroPadding: {
        padding: '0px'
    },
    calendar: {
        textAlign: 'right',
    },
    dateSelected: {
        borderBottom: '0px'
    },
    today: {
        textAlign: 'right',
        borderBottom: '0px'
    },
    datePicker: {
        padding: '0px'
    }
})