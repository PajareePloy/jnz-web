import React from 'react'
import { withStyles } from '@material-ui/core/styles'
import { Avatar, Table, TableRow, TableCell, Typography, Link, Button, Divider, IconButton, TextField } from '@material-ui/core'
import { Today, AccessTime, AttachFile, Redeem, ThumbUpAltOutlined, ChatBubbleOutline, PhotoLibrary, AlternateEmail } from '@material-ui/icons'
import styles from './styles'

class ReportOKR extends React.Component {

    constructor(props) {
        super(props)
    }

    renderContent(title, detail) {
        const { classes } = this.props
        return (
            <div className={classes.contentContainer}>
                <Typography variant='body2' color='secondary'>{title}</Typography>
                <Typography variant='body2'>{detail}</Typography>
            </div>
        )
    }

    renderImage() {
        const { classes } = this.props
        return (
            <div className={classes.contentContainer}>
                <Typography variant='body2' color='secondary'>Image</Typography>
                <img className={classes.largeImage} />
                <img className={classes.largeImage} />
                <img className={classes.largeImage} />
            </div>
        )
    }

    renderAttachFile() {
        const { classes } = this.props
        return (
            <div className={classes.contentContainer}>
                <Typography variant='body2' color='secondary'>Attach File</Typography>
                <AttachFile color='secondary' fontSize='small' /><Link href='#' color='primary'>Update Design OKRs.pdf</Link>
            </div>
        )
    }

    renderSummarizeBar() {
        const { classes } = this.props
        return (
            <Table className={classes.summarizeBar}>
                <TableRow>
                    <TableCell align='left' className={classes.noBorder}>
                        <Typography variant='subtitle2' color='secondary' >Read (12)&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;Unread (25)&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;Like (2)</Typography>
                    </TableCell>
                    <TableCell align='right' className={classes.noBorder}>
                        <Link href='#' color='primary'>{'View >'}</Link>
                    </TableCell>
                </TableRow>
            </Table>
        )
    }

    renderButton() {
        const { classes } = this.props
        return(
            <Table>
                <TableRow>
                    <TableCell align='center' >
                        <Button variant='outlined' color='primary' className={classes.iconButton}>
                            <Redeem />&nbsp;Gift Point
                        </Button>
                    </TableCell>
                    <TableCell align='center' >
                        <Button variant='outlined' color='primary' className={classes.iconButton}>
                            <ThumbUpAltOutlined />&nbsp;Like
                        </Button>
                    </TableCell>
                </TableRow>
            </Table>
        ) 
    }

    renderComment(title, position, comment, period, like) {
        const { classes } = this.props
        return (
            <div>
            <div className={classes.contentContainer}>
                <Table>
                    <TableRow>
                        <TableCell className={classes.miniAvatarContainer}>
                            <Avatar />
                        </TableCell>
                        <TableCell className={classes.zeroPadding}>
                            <Typography variant='body2' >{title}</Typography>
                            <Typography variant='caption'>{position}</Typography>
                        </TableCell>
                    </TableRow>
                </Table>
                <Typography variant='subtitle2' >{comment}</Typography>
                <Table>
                    <TableRow>
                        <TableCell className={classes.zeroPadding}>
                            <Typography variant='caption' color='secondary'>{period}</Typography>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <Typography variant='caption' color='primary'>{like}</Typography>
                        </TableCell>
                        <TableCell align='right' className={classes.zeroPadding}>
                            <ChatBubbleOutline className={classes.miniIcon} color='primary'/><Link variant='caption' color='primary'> Reply</Link>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <ThumbUpAltOutlined className={classes.miniIcon} color='primary'/><Link variant='caption' color='primary'> Like</Link>
                        </TableCell>
                    </TableRow>
                </Table>
            </div>
            <Divider className={classes.divider} />
            </div>
        )
    }

    renderCommentBar() {
        const { classes } = this.props
        return (
            <div className={classes.commentBar}>
                <IconButton edge='start' color='secondary'>
                    <PhotoLibrary />
                </IconButton>
                <IconButton edge='start' color='secondary'>
                    <AttachFile />
                </IconButton>
                <IconButton edge='start' color='secondary'>
                    <AlternateEmail />
                </IconButton>
                <TextField size='small' variant='outlined' label='Comment' />
            </div>
        )
    }

    render() {
        const { classes } = this.props
        return (
            <div>
                <Table>
                    <TableRow>
                        <TableCell className={classes.avatarContainer}>
                            <Avatar className={classes.largeAvatar} />
                        </TableCell>
                        <TableCell className={classes.zeroPadding}>
                            <Typography variant='h6' >Ekarach Sripen</Typography>
                            <Typography variant='body2'>Design Director</Typography>
                        </TableCell>
                        <TableCell align='right' className={classes.noBorder}>
                            <Today color='secondary' className={classes.miniIcon}/><Typography variant='caption' color='secondary' > 14/01/2020</Typography>
                            <br />
                            <AccessTime color='secondary' className={classes.miniIcon}/><Typography variant='caption' color='secondary'> 12:45</Typography>
                        </TableCell>
                    </TableRow>
                </Table>
                {this.renderContent('Content 1', 'Lorem ipsum dolor sit arnet, consectotur adioising edit, sod do aiusmod tempor')}
                {this.renderContent('Content 2', 'Ut enim ad minim voriam, quis nostrudyewr Loren ipsm dolor sit amet, consectotur')}
                {this.renderContent('Content 3', 'Lorem ipsum dolor sit arnet, consectotur adioising edit, sod do aiusmod tempor incididunt ut labore et dolore magna atiqua. Ut enim ad minim veniam, quis nostrud exercitation')}
                {this.renderImage()}
                {this.renderAttachFile()}
                {this.renderSummarizeBar()}
                {this.renderButton()}
                {this.renderComment('Anankin Skywalker', 'Designer', 'Lorem ipsum dolor sit arnet, consectotur adioising edit, sod do aiusmod tempor', '2 days ago', '3 People likes')}
                {this.renderComment('Amanda Lay', 'Designer', 'Lorem ipsum dolor sit arnet, consectotur adioising edit, sod do aiusmod tempor', '2 days ago', 'like')}
                {this.renderCommentBar()}
            </div>
        )
    } 
}
export default (withStyles(styles)(ReportOKR))