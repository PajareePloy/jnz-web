export default (theme) => ({
    largeAvatar: {
        width: theme.spacing(7),
        height: theme.spacing(7),
    },
    zeroPadding: {
        padding: '0px',
        border: '0px'
    },
    avatarContainer: {
        paddingRight: '0px',
        border: '0px'
    },
    miniIcon: {
        width: '12px',
        height: '12px'
    },
    noBorder: {
        border: '0px'
    },
    contentContainer: {
        padding: '8px'
    },
    largeImage: {
        width: theme.spacing(10),
        height: theme.spacing(10),
        backgroundColor: 'lightGrey',
        marginRight: '10px'
    },
    summarizeBar: {
        backgroundColor: '#E3F2FD'
    },
    iconButton: {
        borderRadius: '20px',
        textTransform: 'none',
        width: '150px'
    },
    miniAvatarContainer: {
        paddingRight: '0px',
        border: '0px',
        width: '70px'
    },
    divider: {
        margin: '10px 0px 0px 0px'
    },
    commentBar: {
        padding: '12px',
        textAlign: 'center'
    }
})