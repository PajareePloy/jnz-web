import React from 'react'
import { withStyles } from '@material-ui/core/styles'
import { Table, TableCell, TableRow } from '@material-ui/core'
import { BlurOnOutlined, ChatBubbleOutline, Redeem, ThumbUpAltOutlined } from '@material-ui/icons'
import styles from './styles'

import ReportCard from '../ReportCard'

class ReportDaily extends React.Component {

    constructor(props) {
        super(props)
    }

    render() {
        const { classes } = this.props
        return (
            <Table>
                <TableRow>
                    <TableCell className={classes.cell}>
                        <ReportCard icon={<ThumbUpAltOutlined fontSize='small' />} title='Like' amount={34} unit='Likes' />
                    </TableCell>
                    <TableCell className={classes.cell}>
                        <ReportCard icon={<ChatBubbleOutline fontSize='small' />} title='Comment' amount={56} unit='Comments' />
                    </TableCell>
                </TableRow>
                <TableRow>
                    <TableCell className={classes.cell}>
                        <ReportCard icon={<Redeem fontSize='small' />} title='Point' amount={450} unit='Point' />
                    </TableCell>
                    <TableCell className={classes.cell}>
                        <ReportCard icon={<BlurOnOutlined fontSize='small' />} title='Diamond' amount={40} unit='Diamond' />
                    </TableCell>
                </TableRow>
            </Table>
        )
    } 
}
export default (withStyles(styles)(ReportDaily))