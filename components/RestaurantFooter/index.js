import React from 'react'
import { withStyles } from '@material-ui/core/styles'
import { Typography } from '@material-ui/core'
import styles from './styles'

class RestaurantFooter extends React.Component {

    render() {
        const { classes } = this.props
        return (
            <div className={ classes.container }>
                <Typography variant='h6'>Footer</Typography>
            </div>
        )
    } 
}
export default (withStyles(styles)(RestaurantFooter))