export default (theme) => ({
    container: {
        position: 'absolute',
        overflow: 'hidden',
        bottom: '0px',
        left: '0px',
        right: '0px',
        width: '100%',
        minHeight: '50px',
        borderTop: `solid ${theme.palette.grey[400]} 1px`
    }
})
