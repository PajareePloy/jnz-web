export default (theme) => ({
    container: {
        textAlign: 'center',
        paddingBottom: '20px'
    },
    iconButton: {
        borderRadius: '20px',
        textTransform: 'none',
        height: '24px',
        fontSize: '11px'
    },
    miniAvatarContainer: {
        padding: '0px',
        border: '0px',
        width: '40px'
    },
    zeroPadding: {
        padding: '0px',
        border: '0px'
    },
    showIndex: {
        backgroundColor: theme.palette.common.black,
        width: theme.spacing(3),
        height: theme.spacing(3),
        fontSize: '11px'
    },
    miniContainer: {
        padding: '5px'
    },
    profileIcon: {
        width: theme.spacing(4),
        height: theme.spacing(4),
    },
    miniIcon: {
        width: '10px',
        paddingTop:'14px'
    },
    rightContainer: {
        width: '80px',
        padding: '0px',
        border: '0px'
    }
})