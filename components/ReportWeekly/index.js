import React from 'react'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles'
import { Button, Avatar, Table, TableRow, TableCell, Typography } from '@material-ui/core'
import { BlurOnOutlined, ChatBubbleOutline, Redeem, ThumbUpAltOutlined, ThumbUp, ChatBubbleOutlined, Book } from '@material-ui/icons'
import { LineChart, Line, XAxis, YAxis, CartesianGrid } from 'recharts'
import styles from './styles'

class ReportWeekly extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            viewType: 'Graph',    
            menuSelected: 0,
            startDate: '',
            endDate: ''
        }
    }

    componentDidMount() {
        this.setState({
            viewType: this.props.report.viewType,
            startDate: this.props.report.startDate,
            endDate: this.props.report.endDate
        })
    }

    componentWillReceiveProps = (nextProps) => {
        this.setState({
            viewType: nextProps.report.viewType,
            startDate: nextProps.report.startDate,
            endDate: nextProps.report.endDate
        })
    }

    handleChangeMenu(value) {
        this.setState({
            menuSelected: value
        })
    } 

    renderButtonMenu() {
        const { classes } = this.props
        const { menuSelected } = this.state
        return (
            <div className={classes.container} >
                <Button variant={menuSelected===0?'contained':'outlined'} color='primary' onClick={()=>this.handleChangeMenu(0)} className={classes.iconButton}>
                    <ThumbUpAltOutlined fontSize='small' />&nbsp;Like
                </Button>
                <Button variant={menuSelected===1?'contained':'outlined'} color='primary' onClick={()=>this.handleChangeMenu(1)} className={classes.iconButton}>
                    <ChatBubbleOutline fontSize='small' />&nbsp;Comment
                </Button>
                <Button variant={menuSelected===2?'contained':'outlined'} color='primary' className={classes.iconButton}>
                    <Redeem fontSize='small' />&nbsp;Point
                </Button>
                <Button variant={menuSelected===3?'contained':'outlined'} color='primary' className={classes.iconButton}>
                    <BlurOnOutlined fontSize='small' />&nbsp;Diamond
                </Button>
            </div>
        )
    }

    renderGraphView() {
        const { menuSelected } = this.state
        return ( 
            <div>
                <LineChart width={320} height={220} data={data[menuSelected]}>
                    <XAxis dataKey='date'/>
                    <YAxis/>
                    <CartesianGrid strokeDasharray='7 4'/>
                    <Line dataKey='amt' stroke='#3060FF' />      
                </LineChart>
            </div>
        )
    }

    renderListView() {
        const { classes } = this.props
        const { menuSelected } = this.state 
        const data = detail[menuSelected]

        return data.map((p, index) => {
            return (
                <TableRow>
                    <TableCell className={classes.miniContainer}>
                        <Avatar className={classes.showIndex}>{index}</Avatar>
                    </TableCell>
                    <TableCell className={classes.miniContainer}>
                        <Table >
                            <TableRow>
                                <TableCell className={classes.miniAvatarContainer}>
                                    <Avatar className={classes.profileIcon} />
                                </TableCell>     
                                <TableCell className={classes.zeroPadding}>
                                    <Typography variant='caption'>{p.name}</Typography>
                                    <br />
                                    <Book className={classes.miniIcon} />&nbsp;<Typography variant='caption'>{p.subtitle}</Typography>
                                </TableCell>
                                <TableCell align='left' className={classes.rightContainer}>
                                    {menuSelected === 0 ? <div><ThumbUp color='primary' fontSize='small' /><Typography variant='caption'> {p.amount} People Likes</Typography></div> : <div><ChatBubbleOutlined color='primary' fontSize='small' /><Typography variant='caption'> {p.amount} Comments</Typography></div> }
                                </TableCell>
                            </TableRow>
                        </Table>
                    </TableCell>
                </TableRow>
            )
        })
    }

    render() {
        const { viewType, startDate, endDate } = this.state
        return (
            <div>
                {this.renderButtonMenu()}
                {startDate !== endDate ? viewType === 'Graph' ? this.renderGraphView() : <Table>{this.renderListView()}</Table> : ''}
            </div>
        )
    } 
}
export default connect(state => state)(withStyles(styles)(ReportWeekly))

const data = [[
        {
            date: '10', amt: 56
        },
        {
            date: '11', amt: 34
        },
        {
            date: '12', amt: 64
        },
        {
            date: '13', amt: 36
        },
        {
            date: '14', amt: 56
        },
        {
            date: '15', amt: 45
        },
        {
            date: '16', amt: 49
        }
    ],
    [
        {
            date: '10', amt: 31
        },
        {
            date: '11', amt: 24
        },
        {
            date: '12', amt: 43
        },
        {
            date: '13', amt: 35
        },
        {
            date: '14', amt: 32
        },
        {
            date: '15', amt: 57
        },
        {
            date: '16', amt: 45
        }
    ],
    [
        {
            date: '10', amt: 24
        },
        {
            date: '11', amt: 46
        },
        {
            date: '12', amt: 45
        },
        {
            date: '13', amt: 65
        },
        {
            date: '14', amt: 45
        },
        {
            date: '15', amt: 53
        },
        {
            date: '16', amt: 23
        }
    ],
    [
        {
            date: '10', amt: 53
        },
        {
            date: '11', amt: 64
        },
        {
            date: '12', amt: 33
        },
        {
            date: '13', amt: 53
        },
        {
            date: '14', amt: 64
        },
        {
            date: '15', amt: 23
        },
        {
            date: '16', amt: 48
        }
    ]]

const detail = [[
    { 
        name: 'Edcd Sroper, Design Director',
        subtitle: 'Csae Repasf',
        amount: 30
    },
    { 
        name: 'Asdsadcd a, Creative',
        subtitle: 'As Repasf',
        amount: 23
    },
    { 
        name: 'Osdsedcd Ssdda, HR',
        subtitle: 'Bdsae Tdfdd',
        amount: 11
    },
    { 
        name: 'Asdsadcd a, Graphic Designer',
        subtitle: 'Adss Fdssd',
        amount: 3
    },
    { 
        name: 'Odfwes Usdoper, Graphic Designer',
        subtitle: 'Csae Deasda',
        amount: 45
    },
    { 
        name: 'Passef Asdefc, Creative',
        subtitle: 'As Repasf',
        amount: 23
    }],
    [{ 
        name: 'Edcd Sroper, Creative',
        subtitle: 'Csae Repasf',
        amount: 65
    },
    { 
        name: 'Asdsadcd a, HR',
        subtitle: 'As Repasf',
        amount: 57
    },
    { 
        name: 'Osdsedcd Ssdda, Graphic Designer',
        subtitle: 'Bdsae Tdfdd',
        amount: 75
    },
    { 
        name: 'Asdsadcd a, Creative',
        subtitle: 'Adss Fdssd',
        amount: 54
    },
    { 
        name: 'Odfwes Usdoper, Graphic Designer',
        subtitle: 'Csae Deasda',
        amount: 75
    },
    { 
        name: 'Passef Asdefc, Creative',
        subtitle: 'As Repasf',
        amount: 45
    }
    ]]
