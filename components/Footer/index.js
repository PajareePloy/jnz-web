import React from 'react'
import { withStyles } from '@material-ui/core/styles'
import { BottomNavigation, BottomNavigationAction } from '@material-ui/core'
import { CheckCircle, PieChart, Send, Settings } from '@material-ui/icons'
import styles from './styles'

class Footer extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            selectedMenu: 'Statistic'
        }
    }

    render() {
        const { classes } = this.props
        const { selectedMenu } = this.state

        return (
            <div className={ classes.container }>
                <BottomNavigation showLabels value={ selectedMenu } >
                    <BottomNavigationAction label='Approval' value='Approval' icon={<CheckCircle color={ selectedMenu === 'Approval' ? 'primary' : 'secondary'} fontSize='small' />} className={ classes.iconMenu } />
                    <BottomNavigationAction label='Report' value='Report' icon={<Send color={ selectedMenu === 'Report' ? 'primary' : 'secondary'} fontSize='small' />} className={ classes.iconMenu } />
                    <BottomNavigationAction label='Statistic' value='Statistic' icon={<PieChart color={ selectedMenu === 'Statistic' ? 'primary' : 'secondary'} fontSize='small' />} className={ classes.iconMenu } /> 
                    <BottomNavigationAction label='Setting' value='Setting' icon={<Settings color={ selectedMenu === 'Setting' ? 'primary' : 'secondary'} fontSize='small' />} className={ classes.iconMenu } />
                </BottomNavigation>
            </div>
        )
    } 
}
export default (withStyles(styles)(Footer))