import React from 'react'
import { withStyles } from '@material-ui/core/styles'
import { Card, CardContent, Typography } from '@material-ui/core'
import styles from './styles'

class ReportCard extends React.Component {

    constructor(props) {
        super(props)
    }

    render() {
        const { classes } = this.props
        return (
            <Card className={classes.container} variant='outlined'>
                <CardContent>
                    <Typography variant='subtitle1' className={classes.title} color='primary'>
                        {this.props.icon} {this.props.title}
                    </Typography>
                    <br />
                    <Typography variant='h3' className={classes.pos} >
                        {this.props.amount}
                    </Typography>
                    <Typography variant='body2' color='secondary'>
                        {this.props.unit}
                    </Typography>
                </CardContent>
            </Card>
        )
    } 
}
export default (withStyles(styles)(ReportCard))