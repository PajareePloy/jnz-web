import _ from 'lodash'
import config from './config'
import { firebase } from './firebase'

function stringifyParams(params) {
    const keys = Object.keys(params)
    if (keys.length === 0) {
        return ''
    }
    const q = keys.reduce((acc, key) => {
        if (params[key]) {
            acc += `&${key}=${params[key]}`
        }
        return acc
    }, '')
    return q.substring(1)
}

export async function getToken() {
    const currentUser = await getCurrentUser()
    let accessToken = ''
    if(currentUser) {
        accessToken = currentUser.getIdToken()
    }
    return accessToken
}

export async function getCurrentUser() {
    const currentUser = await firebase.auth().currentUser
    return currentUser
}

export async function getRequest(urlPath, params = {}) {

    const queryString = stringifyParams(params)
    const accessToken = await getToken()

    return new Promise((resolve) => {
        fetch(`${config.baseApiUrl}${urlPath}?${queryString}`, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + accessToken ? accessToken : ''
            }
        })
        .then(res => {
            resolve(res.json())
        })
    })
}

export async function postRequest(urlPath, data) {

    const accessToken = await getToken()

    return new Promise((resolve) => {
        fetch(`${config.baseApiUrl}${urlPath}`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + accessToken ? accessToken : ''
            },
            body: JSON.stringify(data)
        })
        .then(res => {
            resolve(res.json())
        })
    })
}