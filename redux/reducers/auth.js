import { SIGNIN_SUCCESS, SIGNIN_FAIL, SIGNOUT_SUCCESS } from '../actions/types'

const initialState = {
    userName: null
}

export const authReducer = (state=initialState, action) => {
    switch (action.type) {
        case SIGNIN_SUCCESS:
            return { ...state, userName: action.userName }
        case SIGNIN_FAIL:
            return { ...initialState }
        case SIGNOUT_SUCCESS:
            return { ...initialState }
        default:
            return state
    }
}