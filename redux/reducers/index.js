import { combineReducers } from 'redux'
import { authReducer } from './auth'
import { reportReducer } from './report' 
import { restaurantReducer } from './restaurant'

export const reducers = combineReducers({
    auth: authReducer,
    report: reportReducer,
    restaurant: restaurantReducer
})
