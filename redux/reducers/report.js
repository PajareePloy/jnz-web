import { CHANGE_REPORT_TYPE, CHANGE_VIEW_TYPE, CHANGE_DATE } from '../actions/types'

const today = new Date()

const initialState = {
    reportType: 'All',
    viewType: 'Graph',
    dateType: 'Daily',
    startDate: today,
    endDate: today
}

export const reportReducer = (state=initialState, action) => {
    switch (action.type) {
        case CHANGE_REPORT_TYPE:
            return { ...state, reportType: action.reportType }
        case CHANGE_VIEW_TYPE:
            return { ...state, viewType: action.viewType }
        case CHANGE_DATE:
            return { ...state, dateType: action.dateType, startDate: action.startDate, endDate: action.endDate }
        default:
            return state
    }
}