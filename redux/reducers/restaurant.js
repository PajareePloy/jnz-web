import { RESTAURANT_DATA } from '../actions/types'

const initialState = {
    restaurantData: []
}

export const restaurantReducer = (state=initialState, action) => {
    switch (action.type) {
        case RESTAURANT_DATA:
            return { ...state, restaurantData: action.restaurantData }
        default:
            return state
    }
}