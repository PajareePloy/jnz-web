import * as authClient from './auth'
import * as tictactoeClient from './tictactoe'
import * as restaurantClient from './restaurant'

export default {
    auth: authClient,
    tictactoe: tictactoeClient,
    restaurant: restaurantClient
}
