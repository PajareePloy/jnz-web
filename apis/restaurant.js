import { getRequest } from '../common/util'

export function searchRestaurant(searchText) {
    return getRequest(`/public/searchRestaurant/${searchText}`)
}