import { postRequest } from '../common/util'

export function robotTurn(board, turn) {
    const data = { board, turn }
    return postRequest(`/public/ticTacToeRobotTurn`, data)
}