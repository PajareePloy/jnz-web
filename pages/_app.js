import React from 'react'
import App from 'next/app'
import PropTypes from 'prop-types'
import withRedux from 'next-redux-wrapper'
import { Provider } from 'react-redux'
import CssBaseline from '@material-ui/core/CssBaseline'
import { MuiThemeProvider } from '@material-ui/core/styles'

import { initializeStore } from '../redux/store'
import getPageContext from '../lib/getPageContext'
import '../styles/globals.css'

class MyApp extends App {

    constructor(props) {
        super(props)
        this.pageContext = getPageContext()
    }
    
    pageContext = null

     componentDidMount() {
         // Remove the server-side injected CSS.
         const jssStyles = document.querySelector('#jss-server-side')
        if (jssStyles && jssStyles.parentNode) {
             jssStyles.parentNode.removeChild(jssStyles)
         }
     }

    render() {
        const { Component, PageProps, store} = this.props

        return (
            <MuiThemeProvider theme={this.pageContext.theme} sheetsManager={this.pageContext.sheetsManager}>
                <CssBaseline />
                <Provider store={store}>
                    <Component pageContext={this.pageContext} {...PageProps} />
                </Provider>
            </MuiThemeProvider>
        )
    }
}

export default withRedux(initializeStore)(MyApp)

MyApp.propTypes = {
    Component: PropTypes.elementType.isRequired,
    pageProps: PropTypes.object.isRequired
}