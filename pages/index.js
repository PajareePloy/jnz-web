import Head from 'next/head'
import styles from '../styles/Home.module.css'
import config from '../common/config'

export default function Home() {
    return (
        <div className={styles.container}>
            <Head>
                <title>Jenosize Test</title>
                <link rel='icon' href='/favicon.ico' />
            </Head>
            <main className={styles.main}>
                <h1 className={styles.title}>
                    Welcome to Jenosize Test
                </h1>
                <p className={styles.description}>
                    <code className={styles.code}>Developed by Pajaree Asawapayukkul (Ploy)</code>
                </p>
                <div className={styles.grid}>
                    <div className={styles.card}>
                        <a href='/restaurant'><h3>Restaurant Search &rarr;</h3></a>
                        <p>by Google Places API</p>
                        <br />
                        <p><b>API to get JSON</b></p>
                        <p>{config.baseApiUrl}/public/searchRestaurant/:searchText</p>
                    </div>
                    <div className={styles.card}>
                        <h3>Game 24</h3>
                        <p>Put your 4 numbers in API</p>
                        <br />
                        <p><b>API</b></p>
                        <p>{config.baseApiUrl}/public/game24/:num1/:num2/:num3/:num4</p>
                    </div>
                    <div className={styles.doubleCard}>
                        <a href='/signin'><h3>Sign in &rarr;</h3></a>
                        <p>by Firebase Authentication and Middleware Validation</p>
                        <br />
                        <p><b>User for email sign in</b></p>
                        <p>#1: pajaree.asawa@gmail.com / 123456&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;#2: segun.asawa@gmail.com / 0uFojw:9N</p>
                        <p>OR: Create new user by call {config.baseApiUrl}/public/signUpWithEmail/:email/:password/:displayName</p>
                    </div>
                    <div className={styles.littleCard}>
                        <a href='/tictactoe'><h3>Tic Tac Toe &rarr;</h3></a>
                        <p>Play with bot</p>
                    </div>
                    <div className={styles.littleCard}>
                        <a href='/report'><h3>Responsive Report &rarr;</h3></a>
                        <p>by Next.js, Redux, Global Config, Component Mgt.</p>
                    </div>
                </div>
            </main>

            <footer className={styles.footer}>
                <p><b>Git:</b> https://gitlab.com/PajareePloy/jnz-api  &  https://gitlab.com/PajareePloy/jnz-web</p>
            </footer>
        </div>
    )
}
