export default theme => ({
    grid: {
        padding: '10%',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    subGrid: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    button: {
        width: '200px',
        margin: '5px'
    },
    googleButton: {
        width: '200px',
        margin: '5px',
        backgroundColor: '#FF3232',
        color: '#FFFFFF',
        '&:hover': {
            backgroundColor: '#CC0000',
        }
    },
    textFieldContainer: {
        margin: '10px'
    },
    msg: {
        padding: '5px'
    },
    mainMsgError: {
        padding: '5px',
        textAlign: 'center'
    }
})