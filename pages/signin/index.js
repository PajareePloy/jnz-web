import React from 'react'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles'
import { Typography, Grid, Button, Divider, TextField, FormGroup} from '@material-ui/core'
import { getCurrentUser } from '../../common/util'


import { auth, firebase } from '../../common/firebase'
import { SIGNIN_SUCCESS, SIGNOUT_SUCCESS } from '../../redux/actions/types'
import apiClient from '../../apis'

import styles from './styles'

class SignIn extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            email: '',
            password: '',
            error: '',
            userName: '',
            mainError: '',
            apiResult: ''
        }
    }

    initState = () => {
        this.setState({
            email: '',
            password: '',
            error: '',
            userName: '',
            mainError: '',
            apiResult: ''
        })
    }

    componentDidMount = async () => {
        this.initState()
        const res = await getCurrentUser()
        if (res) {
            this.setState({
                userName: res.displayName
            })
        }
    }

    componentWillReceiveProps = (nextProps) => {
        this.initState()
        this.setState({
            userName: nextProps.auth.userName
        })
    }

    handleChange = (name) => (event) => {
        this.setState({
            error: ''
        })
        this.setState({
            [name]: event.target.value,
        })
    }
    
    emailSignIn = async () => {
        try {
            const { email, password } = this.state
            const result = await firebase.auth().signInWithEmailAndPassword(email, password)
            this.props.dispatch({ type: SIGNIN_SUCCESS, userName: result.user.displayName })
        }
        catch (err) {
            console.log(err)
            this.setState({
                error: 'Incorrect email or password.'
            })
        }
    }

    specialSignIn = async (type) => {
        try {
            let provider 
            if (type === 'GOOGLE') {
                provider = new firebase.auth.GoogleAuthProvider()
            }
            else if (type === 'FACEBOOK') {
                provider = new firebase.auth.FacebookAuthProvider()
            }
            const result = await firebase.auth().signInWithPopup(provider)
            this.props.dispatch({ type: SIGNIN_SUCCESS, userName: result.user.displayName })
        } 
        catch (err) {
            console.log(err)
            this.setState({
                mainError: `Sign In with ${type} Error.`
            })
        }
    }

    signOut = async () => {
        try {
            await firebase.auth().signOut()
            this.props.dispatch({ type: SIGNOUT_SUCCESS })
        }
        catch (err) {
            console.log(err)
            this.setState({
                mainError: 'Sign Out Error.'
            })
        }
    }

    testAPI = async () => {
        const result = await apiClient.auth.testMiddleWare()
        let msg = ''
        if (result.message) {
            msg = result.message
        }
        else {
            msg = result.error
        }
        this.setState({
            apiResult: msg
        })
    }

    renderSignIn(classes) {
        const { mainError } = this.state
        return (
            <div>
                <Grid item className={ classes.grid}>
                    <Grid container justify='center' spacing={5}>
                        <Grid item className={ classes.subGrid }>
                            <FormGroup className={ classes.textFieldContainer }>
                                <TextField
                                    label='Email'
                                    value={this.state.email}
                                    onChange={this.handleChange('email')}
                                />
                                <TextField
                                    label='Password'
                                    type='password'
                                    value={this.state.password}
                                    onChange={this.handleChange('password')}
                                />
                            </FormGroup>
                            <div>
                                <Typography color='error' variant='subtitle2' className={ classes.msg }>{this.state.error}</Typography>
                                <Button variant='contained' onClick={ this.emailSignIn } className={ classes.button }>
                                    Sign in
                                </Button>
                            </div>
                        </Grid>
                        <Divider orientation='vertical' flexItem />
                        <Grid item className={ classes.subGrid }>
                            <Grid item>
                                <Button variant='contained' onClick={ () => this.specialSignIn('GOOGLE') } className={ classes.googleButton }>
                                    Google Sign In
                                </Button>
                            </Grid>
                            <Grid item>
                                <Button variant='contained' color='primary' onClick={ () => this.specialSignIn('FACEBOOK') } className={ classes.button }>
                                    Facebook Sign In
                                </Button>
                            </Grid>
                        </Grid>
                    </Grid> 
                </Grid>
                <Typography color='error' variant='subtitle2' className={ classes.mainMsgError }>{ mainError }</Typography>
            </div>
        )
    } 

    renderSignOut(classes) {
        const { userName, mainError } = this.state
        return (
            <Grid item className={ classes.grid }>
                <Typography variant='h4' className={ classes.msg }>Welcome, { userName }</Typography>
                <Button variant='contained' color='primary' onClick={ this.signOut } className={ classes.button }>
                    Sign out
                </Button>
                <Typography color='error' variant='subtitle2' className={ classes.mainMsgError }>{ mainError }</Typography>
            </Grid>
        )
    }

    renderTestAuthorized(classes) {
        return (
            <div className={ classes.subGrid }>
                <Typography variant='subtitle2' className={ classes.msg }>Test Call API with Authorization Request</Typography>
                <Button variant='contained' onClick={ this.testAPI } className={ classes.button }>
                    Test
                </Button>
                <Typography variant='subtitle2' className={ classes.msg }>{ this.state.apiResult }</Typography>
            </div>
        )
    }

    render() {
        const { classes } = this.props
        const { userName } = this.state
        if (userName) {
            return (
                <div>
                    {this.renderSignOut(classes)}
                    {this.renderTestAuthorized(classes)}
                </div>
            )
        }
        return (
            <div>
                {this.renderSignIn(classes)}
                {this.renderTestAuthorized(classes)}
            </div>
        )
    }
}

export default connect(state => state)(withStyles(styles)(SignIn))