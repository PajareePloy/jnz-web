import React from 'react'
import { withStyles } from '@material-ui/core/styles'
import { Avatar, ButtonGroup, Button, Typography } from '@material-ui/core'

import styles from './styles'
import apiClient from '../../apis'

//Some constant should convert and keep in constant file.

class TicTacToe extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            board: ['', '', '', '', '', '', '', '', '',],
            userPlayer: '',
            start: false,
            turn: '',
            status: ''
        }
    }

    findIndex(row, column) {
        return (row * 3) + column
    }

    handleCellClick = async (row, column) => {
        const { board, turn, userPlayer } = this.state 
        const index = this.findIndex(row, column)
        if (board[index] === '' && turn === userPlayer && status === '') {
            const newBoard = [...this.state.board]
            newBoard[index] = userPlayer
            const result = await this.checkWin(newBoard, turn)
            if (result === '') {
                const updTurn = this.state.turn === 'X' ? 'O' : 'X'
                this.setState({
                    board: newBoard,
                    turn: updTurn
                })
                this.robotTurn(newBoard, updTurn)
            }
            else {
                let text
                if (result === 'W') {
                    text = 'Win'
                } 
                else if (result === 'D') {
                    text = 'Draw'
                }
                this.setState({
                    board: newBoard,
                    status: text
                })
            }
        }
    }

    robotTurn = async (board, turn) => {
        const res = await apiClient.tictactoe.robotTurn(board, turn)
        let newBoard = [...this.state.board]
        newBoard[res.index] = this.state.turn
        this.setState({
            board: newBoard,
        })
        const result = await this.checkWin(newBoard, turn)
        if (result === '') {
            this.setState({
                turn: this.state.turn === 'X' ? 'O' : 'X'
            })
        }
        else {
            let text
            if (result === 'W') {
                text = 'Lose'
            } 
            else if (result === 'D') {
                text = 'Draw'
            }
            this.setState({
                status: text
            })
        }
    }

    checkWin(board, turn) {    
        if ((board[0] === turn && board[1] === turn && board[2] === turn) ||
            (board[3] === turn && board[4] === turn && board[5] === turn) ||
            (board[6] === turn && board[7] === turn && board[8] === turn) ||
            (board[0] === turn && board[3] === turn && board[6] === turn) ||
            (board[1] === turn && board[4] === turn && board[7] === turn) ||
            (board[2] === turn && board[5] === turn && board[8] === turn) ||
            (board[0] === turn && board[4] === turn && board[8] === turn) ||
            (board[2] === turn && board[4] === turn && board[6] === turn)) {
                return 'W'
        } else {
            const emptyCell = board.filter(s => s === '')
            if (emptyCell.length === 0) {
                return 'D'
            }
            else {
                return ''
            }
        }
    } 

    startGame(player) {
        this.setState({
            start: true,
            turn: 'X',
            userPlayer: player
        })
        if (player !== 'X') {
            this.robotTurn(this.state.board, 'X')
        }
    }

    renderCell(row, column) {
        const { classes } = this.props
        const index = this.findIndex(row, column)
        const data = this.state.board[index]
        return (
            <td className={ classes.column } onClick={ () => this.handleCellClick(row, column) } >
                { data === 'X' ? <Avatar className={ classes.avatar } src='/static/images/X.png' /> : data === 'O' ? <Avatar className={ classes.avatar } src='/static/images/O.png' /> : '' }
            </td> 
        )
    }

    renderGame(classes) {
        return (
            <table className={ classes.table } >
                <tbody>
                    <tr>
                        { this.renderCell(0, 0) }
                        { this.renderCell(0, 1) }
                        { this.renderCell(0, 2) }
                    </tr>
                    <tr>
                        { this.renderCell(1, 0) }
                        { this.renderCell(1, 1) }
                        { this.renderCell(1, 2) }
                    </tr>
                    <tr>
                        { this.renderCell(2, 0) }
                        { this.renderCell(2, 1) }
                        { this.renderCell(2, 2) }
                    </tr>
                </tbody>
            </table>
        )
    }

    renderPlayer(classes) {
        const { start, userPlayer, turn, status } = this.state
        if (start) {
            return (
                <div className={ classes.center }>
                    <Typography variant='subtitle2' >You are Player { userPlayer }.</Typography>
                    <Typography variant='subtitle2' >{ status === '' ? (turn === userPlayer ? 'Your turn.' : 'Robot turn.') : '' }</Typography>
                    <Typography variant='subtitle2' >{ status }</Typography>
                </div>
            )
        }
        else {
            return (
                <div className={ classes.center }>
                    <Typography variant='subtitle2' >Choose Player</Typography>
                    <ButtonGroup variant='contained' color='primary'>
                        <Button onClick={ () => this.startGame('X') } >Player: X</Button>
                        <Button onClick={ () => this.startGame('O') } >Player: O</Button>
                    </ButtonGroup>
                </div>
            )
        }
    }

    render() {
        const { classes } = this.props
        const { start } = this.state

        return (
            <div className={ classes.container }>
                { this.renderPlayer(classes) }
                <br />
                { start ? this.renderGame(classes) : '' }
            </div>
        )
    } 
}

export default withStyles(styles)(TicTacToe)