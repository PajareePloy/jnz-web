export default theme => ({
    container: {
        minHeight: '100vh',
        padding: '0 0.5rem',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    center: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    table: {
        borderCollapse: 'collapse'
    },
    column: {
        width: '150px',
        height: '150px',
        borderStyle: 'solid'
    },
    avatar: {
        height: '150px',
        width: '150px'
    }
})