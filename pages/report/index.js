import React from 'react'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles'
import styles from './styles'

import ReportLayout from '../../layout/ReportLayout'
import TopMenu from '../../components/TopMenu'
import ReportOKR from '../../components/ReportOKR'
import ReportDaily from '../../components/ReportDaily'
import ReportWeekly from '../../components/ReportWeekly'

class Report extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            reportType: '',
            dateType: ''
        }
    }
    
    componentDidMount() {
        this.setState({
            reportType: this.props.report.reportType,
            dateType: this.props.report.dateType
        })
    }

    componentWillReceiveProps = (nextProps) => {
        this.setState({
            reportType: nextProps.report.reportType,
            dateType: nextProps.report.dateType
        })
    }

    renderOKR() {
        return (
            <ReportOKR />
        )
    }

    renderAll() {
        const { dateType } = this.state
        return (
            <div>
                <TopMenu />
                { dateType === 'Daily' ? <ReportDaily /> : <ReportWeekly />}
            </div>
        )
    }

    render() {
        const { classes } = this.props
        const { reportType } = this.state

        return (
            <ReportLayout hideFooter={reportType === 'OKR' ? true : false}>
                <div className={reportType === 'OKR' ? classes.longContainer : classes.container}>
                    { reportType === 'All' ? this.renderAll() : this.renderOKR() }
                </div>
            </ReportLayout>
        )
    }
}

export default connect(state => state)(withStyles(styles)(Report))