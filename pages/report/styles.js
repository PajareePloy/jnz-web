export default theme => ({
    container: {
        position: 'absolute',
        overflow: 'auto',
        top: '100px',
        bottom: '50px',
        left: '0px',
        right: '0px'
    },
    longContainer: {
        position: 'absolute',
        overflow: 'auto',
        top: '100px',
        bottom: '0px',
        left: '0px',
        right: '0px'
    }
})