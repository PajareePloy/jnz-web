import React from 'react'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles'
import styles from './styles'
import _ from 'lodash'

import RestaurantLayout from '../../layout/RestaurantLayout'
import RestaurantDetail from '../../components/RestaurantDetail'

class Restaurant extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            restaurantData: {}
        }
    }

    componentDidMount() {
        this.setState({
            restaurantData: this.props.restaurant.restaurantData
        })
    }

    componentWillReceiveProps = (nextProps) => {
        this.setState({
            restaurantData: nextProps.restaurant.restaurantData
        })
    }
    
    render() {
        const { classes } = this.props
        const { restaurantData } = this.state

        return (
            <RestaurantLayout>
                <div className={classes.container}>
                { !restaurantData ? '' : 
                    _.map(restaurantData, (data, index) => (

                     
                             <RestaurantDetail title={data.name} detail={data.formatted_address} />
                         

                    ))
                }
                </div>
            </RestaurantLayout>
        )
    }
}

export default connect(state => state)(withStyles(styles)(Restaurant))

const mockData = [{"business_status":"OPERATIONAL","formatted_address":"Mu Ban Areeya Mova Alley, Chorakhe Bua, Lat Phrao, Krung Thep Maha Nakhon 10230, Thailand","geometry":{"location":{"lat":13.8474494,"lng":100.6106006},"viewport":{"northeast":{"lat":13.84880517989272,"lng":100.6119501298927},"southwest":{"lat":13.84610552010728,"lng":100.6092504701073}}},"icon":"https://maps.gstatic.com/mapfiles/place_api/icons/v1/png_71/restaurant-71.png","name":"Red Monkey Fries","opening_hours":{"open_now":true},"photos":[{"height":1108,"html_attributions":["<a href=\"https://maps.google.com/maps/contrib/111583214380407274505\">A Google User</a>"],"photo_reference":"CmRaAAAAkM8y7ofnYc1fw5AvOvq6a6t9Zq3NnbJwgdMXZ_fcK_WzqKLA9ZxGoo7SQxUaxWlv-5TLo10OpqulEr95SFFscdkvTI5q3N6Z3vFG4wlst22u7-riU_wpoRMDXwXhnVufEhAzrs3O_bQAGJQHXwm6YftBGhTXZzUjuhMQs2NjgsSJflv1WgML9Q","width":1478}],"place_id":"ChIJPXP7tjWd4jARITXW7ulPM-g","plus_code":{"compound_code":"RJW6+X6 Bangkok","global_code":"7P52RJW6+X6"},"rating":4.3,"reference":"ChIJPXP7tjWd4jARITXW7ulPM-g","types":["restaurant","food","point_of_interest","establishment"],"user_ratings_total":12},{"business_status":"OPERATIONAL","formatted_address":"Chatuchot 8/9 Mahome Square , Blog 11 Aor Ngeon, Sai Mai, Bangkok 10220, Thailand","geometry":{"location":{"lat":13.8977508,"lng":100.6733234},"viewport":{"northeast":{"lat":13.89924372989272,"lng":100.6746607298927},"southwest":{"lat":13.89654407010728,"lng":100.6719610701073}}},"icon":"https://maps.gstatic.com/mapfiles/place_api/icons/v1/png_71/restaurant-71.png","name":"MOJO Burger","opening_hours":{"open_now":true},"photos":[{"height":2000,"html_attributions":["<a href=\"https://maps.google.com/maps/contrib/101540572284140636198\">A Google User</a>"],"photo_reference":"CmRaAAAAMysLCC-GlUvpYTohUGSqxlPRbxBq43kSUasWeZzq2kQF1bjnDOm7z5maizty7gj_tuEl1Vf8URDXLmJJA48uZ8Ah-idQfnyorT5pCzWVNkrKeYyz2uOc1W3Z8aEzhwJKEhAsET7zFWtW0bwcWXkq3uQGGhRIWn4oQwDa1d-bb25SzWttKpt20g","width":3554}],"place_id":"ChIJA8IIiax9HTERf4v6LOViZJc","plus_code":{"compound_code":"VMXF+48 Bangkok","global_code":"7P52VMXF+48"},"rating":4.6,"reference":"ChIJA8IIiax9HTERf4v6LOViZJc","types":["restaurant","food","point_of_interest","store","establishment"],"user_ratings_total":11},{"business_status":"OPERATIONAL","formatted_address":"Lat Yao, Chatuchak, Bangkok 10220, Thailand","geometry":{"location":{"lat":13.8448186,"lng":100.5703111},"viewport":{"northeast":{"lat":13.84616817989272,"lng":100.5717299298927},"southwest":{"lat":13.84346852010728,"lng":100.5690302701073}}},"icon":"https://maps.gstatic.com/mapfiles/place_api/icons/v1/png_71/restaurant-71.png","name":"Burger 4 U","opening_hours":{"open_now":true},"photos":[{"height":2976,"html_attributions":["<a href=\"https://maps.google.com/maps/contrib/105324295136408351070\">woramate jumroonsilp</a>"],"photo_reference":"CmRaAAAABce1C08uQA6jLPdvozY1VL-3XyGnbDhMITZ9r9EsFkZYnS1nS20SheVYqPBpe8wpjweuw41XOCJZWSsfZDYKBokYLs0IDw8cg3Aygs2kKTzbiaq5icjlbQhXW9CFzwzQEhC0rbZ28XLL5m_wkydG7YptGhQnD6m6NpO9X4sNXAusL1Em5q42kg","width":3968}],"place_id":"ChIJE-Jz3uCc4jAR5VaGz2JIC2M","plus_code":{"compound_code":"RHVC+W4 Bangkok","global_code":"7P52RHVC+W4"},"rating":4.2,"reference":"ChIJE-Jz3uCc4jAR5VaGz2JIC2M","types":["restaurant","food","point_of_interest","establishment"],"user_ratings_total":17}]