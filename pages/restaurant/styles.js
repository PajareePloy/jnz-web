export default theme => ({
    container: {
        position: 'absolute',
        overflow: 'auto',
        top: '150px',
        bottom: '50px',
        left: '0px',
        right: '0px'
    }
})